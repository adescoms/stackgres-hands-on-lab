#!/bin/bash

source ./variables

gsutil -m rm \
    -r "gs://${GCLOUD_BACKUP_BUCKET}/"