#!/bin/bash -e

source ./variables

gcloud container \
 --project ${GCP_PROJECT} clusters create ${K8S_CLUSTER_NAME} \
 --region ${GCP_REGION} \
 --no-enable-basic-auth \
 --cluster-version "1.16.13-gke.1" \
 --machine-type "e2-standard-4" \
 --image-type "COS" \
 --disk-type "pd-standard" \
 --disk-size "100" \
 --num-nodes "3" \
 --node-locations "us-central1-a","us-central1-b","us-central1-c" \
 --workload-pool="${GCP_PROJECT}.svc.id.goog" \
 --addons ConfigConnector \
 --enable-stackdriver-kubernetes
