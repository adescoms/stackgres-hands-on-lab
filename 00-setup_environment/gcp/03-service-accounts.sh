#!/bin/bash

source ./variables

kubectl create namespace ${CLUSTER_NAMESPACE}

kubectl create serviceaccount --namespace ${CLUSTER_NAMESPACE} ${GCLOUD_K8S_SERVICE_ACCOUNT}

gcloud iam service-accounts create ${GCLOUD_K8S_SERVICE_ACCOUNT}

## grant access to the bucket
gsutil iam ch \
  serviceAccount:${GCLOUD_K8S_SERVICE_ACCOUNT}@${GCP_PROJECT}.iam.gserviceaccount.com:roles/storage.objectAdmin \
  "gs://${GCLOUD_BACKUP_BUCKET}/"

gcloud iam service-accounts keys \
  create my-creds.json --iam-account ${GCLOUD_K8S_SERVICE_ACCOUNT}@${GCP_PROJECT}.iam.gserviceaccount.com

## create secret
kubectl --namespace ${CLUSTER_NAMESPACE} create secret \
  generic ${GCLOUD_BACKUP_CREDENTIALS_K8S_SECRET} \
	--from-file="my-creds.json"

rm -rfv my-creds.json
