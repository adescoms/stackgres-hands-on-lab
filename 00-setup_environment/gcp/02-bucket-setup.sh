#!/bin/bash

source ./variables

gsutil mb \
    -p ${GCP_PROJECT} \
    -b on \
    -l ${GCP_REGION} \
    "gs://${GCLOUD_BACKUP_BUCKET}/"
