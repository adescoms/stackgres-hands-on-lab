# Hands On Lab


## Prerequisites

For this Hands-On-Lab you just need the three following tools:

* [kubectl](https://kubernetes.io/docs/tasks/tools/), the Kubernetes CLI.
* [Helm](https://helm.sh/docs/intro/install/). Helm v3 is required.
* [Git](https://git-scm.com/downloads). To clone this repository (optional).

You may also benefit from running on a Linux or Linux/UNIX-like environment, with the usual UNIX tools.


## 0. Setting up a Kubernetes Cluster

You obviously need a Kubernetes cluster to run this tutorial. In general, any Kubernetes-compliant cluster versions 1.16, 1.17, 1.18 or 1.19 should work. Some Kubernetes clusters require some specific adjustments. Please see [StackGres documentation on K8s environments](https://stackgres.io/doc/latest/install/prerequisites/k8s/) for specific notes on RKE.

The lab demo will be performed on an [Amazon EKS](https://aws.amazon.com/eks/) cluster. If you wish to create also an EKS environment, you may run the steps detailed below (you will need to have installed [awscli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) and [eksctl](https://github.com/weaveworks/eksctl/releases)):

```
export AWS_REGION= #your preferred region
export K8S_CLUSTER_NAME=	# EKS cluster name

eksctl --region $AWS_REGION create cluster --name $K8S_CLUSTER_NAME \
        --node-type m5a.2xlarge --node-volume-size 100 --nodes 3 \
        --zones ${AWS_REGION}a,${AWS_REGION}b,${AWS_REGION}c \
        --version 1.17
```

This operation takes a bit more than 15 minutes.


## 1. Installing StackGres dependencies

While this is an optional step, it is recommended and it will be followed for the lab. The purpose is to install Prometheus and Grafana (along with AlertManager), so that they can be integrated automatically with StackGres.

Create a `monitoring` namespace:

```
kubectl create namespace monitoring
```

And install the Prometheus stack with Helm:

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm install --namespace monitoring \
        prometheus prometheus-community/kube-prometheus-stack \
        --version 13.4.0 \
        --set grafana.enabled=true
```

After some seconds / a minute you should have several pods including Prometheus, Grafana and AlertManager in the `monitoring` namespace.

You may use a custom Prometheus and Grafana installation if you wish. In this case note the credentials used for accesing them, as will be required when installing StackGres.


## 2. Installing StackGres

First, let's create a namespace to install StackGres controller and the REST API (with the Web Console):

```
kubectl create namespace stackgres
```

StackGres can be installed with Helm. Some parameters may be passed to the default installation, which basically can be summarized as:
* Username and password to access Grafana (this is used by StackGres to install StackGres specific dashboards as well as to embed Grafana into the Web Console). If you installed Prometheus following the previous step, they will be at their default values (username: `admin`, password: `prom-operator`). Also the Grafana host where it is running (by default, exposed as a service at `prometheus-grafana.namespace`, i.e. `prometheus-grafana.monitoring` here).
* How to expose the Web Console. Select `LoadBalancer` if using a cloud Kubernetes cluster or your Kubernetes environment supports creating load balancers. Otherwise, select `ClusterIP` (in this case you will later need to do a port forward to access the Web Console).

Proceed to install StackGres:

```
helm install --namespace stackgres stackgres-operator \
        --set grafana.autoEmbed=true \
        --set-string grafana.webHost=prometheus-grafana.monitoring \
        --set-string grafana.user=admin \
        --set-string grafana.password=prom-operator \
        --set-string adminui.service.type=LoadBalancer \
        https://stackgres.io/downloads/stackgres-k8s/stackgres/1.0.0-beta1/helm/stackgres-operator.tgz
```

After a minute or so you should see the following message:

```
NAME: stackgres-operator
LAST DEPLOYED: Wed Jun 30 13:06:42 2021
NAMESPACE: stackgres
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Release Name: stackgres-operator
StackGres Version: 1.0.0-beta1

   _____ _             _     _____
  / ____| |           | |   / ____|
 | (___ | |_ __ _  ___| | _| |  __ _ __ ___  ___
  \___ \| __/ _` |/ __| |/ / | |_ | '__/ _ \/ __|
  ____) | || (_| | (__|   <| |__| | | |  __/\__ \
 |_____/ \__\__,_|\___|_|\_\\_____|_|  \___||___/
                                  by OnGres, Inc.

Check if the operator was successfully deployed and is available:

    kubectl describe deployment -n stackgres stackgres-operator

    kubectl wait -n stackgres deployment/stackgres-operator --for condition=Available

Check if the restapi was successfully deployed and is available:

    kubectl describe deployment -n stackgres stackgres-restapi

    kubectl wait -n stackgres deployment/stackgres-restapi --for condition=Available

To access StackGres Operator UI from localhost, run the below commands:

    POD_NAME=$(kubectl get pods --namespace stackgres -l "app=stackgres-restapi" -o jsonpath="{.items[0].metadata.name}")

    kubectl port-forward "$POD_NAME" 8443:9443 --namespace stackgres

Read more about port forwarding here: http://kubernetes.io/docs/user-guide/kubectl/kubectl_port-forward/

Now you can access the StackGres Operator UI on:

https://localhost:8443

To get the username, run the command:

    kubectl get secret -n stackgres stackgres-restapi --template '{{ printf "username = %s\n" (.data.k8sUsername | base64decode) }}'

To get the generated password, run the command:

    kubectl get secret -n stackgres stackgres-restapi --template '{{ printf "password = %s\n" (.data.clearPassword | base64decode) }}'

Remember to remove the generated password hint from the secret to avoid security flaws:

    kubectl patch secrets --namespace stackgres stackgres-restapi --type json -p '[{"op":"remove","path":"/data/clearPassword"}]'
```

You may also see the pods deployed by StackGres:

```
$ kubectl -n stackgres get pods
NAME                                  READY   STATUS    RESTARTS   AGE
stackgres-operator-6fb7d568d4-79crr   1/1     Running   0          3m30s
stackgres-restapi-6458d9fdbf-kg9zx    2/2     Running   0          3m28s
```


## 3. Creating a simple StackGres cluster

### 3.1. Cluster creation

StackGres is designed with simplicity in mind. Creating a basic cluster just needs a few lines of very straightforward YAML. Create the following YAML file, name it `simple.yaml` for example and _apply_ it:

```yaml
apiVersion: stackgres.io/v1
kind: SGCluster
metadata:
  name: simple
spec:
  instances: 2
  postgresVersion: 'latest'
  pods:
    persistentVolume: 
      size: '5Gi'
```

The contents of the file should be easily understandable. A StackGres cluster with name `simple` will be created in the `default` namespace (none specified), with two StackGres instances (which will lead to two pods), using the `latest` Postgres version --this is a handy shortcut you may use. StackGres will internally resolve this to the latest version available, and modify your CR to note the specific version used. For each pod, a 5Gi _PersistentVolume_ will be created and attached to the pods.

```
$ kubectl apply -f simple.yaml
sgcluster.stackgres.io/simple created
```

Two Postgres instances (pods) will be created. You can track the progress with:

```
$ kubectl get pods --watch
NAME       READY   STATUS     RESTARTS   AGE
simple-0   0/6     Pending    0          5s
simple-0   0/6     Pending    0          6s
simple-0   0/6     Init:0/5   0          6s
simple-0   0/6     Init:1/5   0          26s
...
```

When the process finished, you should see two pods, each with 6 containers:

```
$ kubectl get pods
NAME       READY   STATUS    RESTARTS   AGE
simple-0   6/6     Running   0          82s
simple-1   6/6     Running   0          36s
```

You should be able to see the resolved Postgres version and other details about your StackGres cluster by inspecting the created CR (Custom Resource):

```
kubectl describe sgcluster simple
```


### 3.2. Accessing Postgres

Now it's time to connect to Postgres. In its simplest form, we can do that by running the Postgres command line, `psql`, in one of the containers of the pod. StackGres creates within the pods a container called `postgres-util` with usual Postgres administration tools, including `psql`. Since this container runs in the same pod, it uses Unix Domain Sockets to connect to the Postgres instance, and leverages Postgres `peer` authentication method, that will authenticate the user via the operating system username, without requiring any password:

```
$ kubectl exec -it simple-0 -c postgres-util -- psql
psql (12.6 OnGres Inc.)
Type "help" for help.

postgres=# 

```

Here you have your Postgres database! Feel free to play around with it. You can exit using CTRL+D or `\q`.

We can actually create a database and some data. Within the previous Postgres console you can run the following commands

```sql
create database hol;
\c hol
create table hol1 as select * from generate_series(1,1000*1000);
\dt+
```

Now connect to the replica and verify that it is in recovery (i.e. read-only mode) and that it has replicated the database and the data:

```
$ kubectl exec -it simple-1 -c postgres-util -- psql
```

(note, we're connecting to `simple-1`, the replica! If `simple-1` is not the replica (it may happen), see the next section below to consult with Patroni who is the replica).

```sql
postgres=# select pg_is_in_recovery();
 pg_is_in_recovery 
-------------------
 t
(1 row)

postgres=# \c hol
You are now connected to database "hol" as user "postgres".
hol=# \dt+
                   List of relations
 Schema | Name | Type  |  Owner   | Size  | Description 
--------+------+-------+----------+-------+-------------
 public | hol1 | table | postgres | 35 MB | 
(1 row)
```


### 3.3. Patroni and automated failover

StackGres uses internall a software called [Patroni](https://github.com/zalando/patroni) to handle high availability and automated failover. It's handled for you, you shouldn't even care about it. Feel free to skip this section if you don't want to know about Patorni. But if you do, you can query Patroni status easily by running `patronictl` on `patroni`'s container:

```
$ kubectl exec -it simple-0 -c patroni -- patronictl list 
+ Cluster: simple (6979461716096839850) ---+---------+----+-----------+
| Member   | Host                | Role    | State   | TL | Lag in MB |
+----------+---------------------+---------+---------+----+-----------+
| simple-0 | 192.168.59.169:7433 | Leader  | running |  1 |           |
| simple-1 | 192.168.12.150:7433 | Replica | running |  1 |         0 |
+----------+---------------------+---------+---------+----+-----------+
```

Here you can see the two nodes, that `simple-0` is the leader node, the Postgres timeline and the lag. Let's trigger now a failover. For example, let's kill the `simple-0` pod:

```
$ kubectl delete pod simple-0
pod "simple-0" deleted
```

When killed, two things will happen in parallel:
* Kubernetes will create a new pod, which will take over `simple-0`. It will attach the previous disk to this node. This operation may be quite fast (a few seconds).
* The replica (`simple-1`) may or may not fail to see the leader renewing its lock. This is due to the timeout that govern when a leader lock expires. If the pod creation operation takes less time than the lock to expire, the new pod will take over the lock quickly enough to "hold" it, and `simple-0` will remain the primary. For a few seconds, there was no primary. However, the new `simple-0` node will be promoted to a new timeline (2), which will be shown in the Patroni state. If, however, the lock expired before `simple-0` was re-created, `simple-1` will be elected as the new leader.

In any case, the situation should restore back to normal, just with a timeline increased and a potential inversion of the Leader. You may repeat this process as you wish, killing either a primary or replica pod.

```
$ kubectl exec -it simple-0 -c patroni -- patronictl list 
+ Cluster: simple (6979461716096839850) ---+---------+----+-----------+
| Member   | Host                | Role    | State   | TL | Lag in MB |
+----------+---------------------+---------+---------+----+-----------+
| simple-0 | 192.168.40.142:7433 | Leader  | running |  2 |           |
| simple-1 | 192.168.12.150:7433 | Replica | running |  2 |         0 |
+----------+---------------------+---------+---------+----+-----------+
```


### 3.4. Connecting to the Web Console

StackGres comes with a fully-featured Web Console. Everything that you do from the CLI will be shown in the Web Console. And viceversa: anything that you run on the Web Console you will be able to check it via `kubectl`. The key is that all StackGres interaction is done through one or more of the existing [StackGres CRDs](https://stackgres.io/doc/latest/reference/crd/).

The Web Console was installed when we installed StackGres. It is publised as a Kubernetes service, which will be typically of type `LoadBalancer` or `ClusterIP`. List the service:

```
$ kubectl -n stackgres get svc
NAME                 TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)         AGE
stackgres-operator   ClusterIP      10.100.110.214   <none>                                                                    443/TCP         8h
stackgres-restapi    LoadBalancer   10.100.208.149   a6716e8a6c97745bd92df7e40c9f1741-1522778690.eu-west-3.elb.amazonaws.com   443:32260/TCP   8h
```

The Web Console is exposed as part of the `stackgres-restapi` service. Here we can see it is of type `LoadBalancer`, and exposed at a given URL. Open this URL in the web browser prefixing it with `https://` , as in:

```
https://a6716e8a6c97745bd92df7e40c9f1741-1522778690.eu-west-3.elb.amazonaws.com
```

If your service is exposed as `ClusterIP`, you can instead use port forwarding to access the Web Console. Find the name of the `restapi` pod in either of the following ways, at your preference:

```
$ kubectl -n stackgres get pods                               
NAME                                  READY   STATUS    RESTARTS   AGE
stackgres-operator-6fb7d568d4-rg7kx   1/1     Running   0          8h
stackgres-restapi-776dd95f65-clfcl    2/2     Running   0          8h

$ POD_NAME=stackgres-restapi-776dd95f65-clfcl
```

```
$ POD_NAME=$(kubectl get pods --namespace stackgres -l "app=stackgres-restapi" -o jsonpath="{.items[0].metadata.name}")
```

And then do the port-forward to your localhost on port 8443, which you can access from the web browser on URL `https://localhost:8443`:

```
$ kubectl port-forward "$POD_NAME" 8443:9443 --namespace stackgres
```

Once you open the Web Console in the browser, you will need to accept to continue to the page. StackGres Web Console uses by default a self-signed certificate, which is generated during the installation. You can customize it during the installation, or install the Web Console only via HTTP and expose it via an _Ingress_ controller. But for now, proceed. The default administrator's username is `admin`, and the password is generated automatically but can be obtained via the following command:

```
$ kubectl get secret -n stackgres stackgres-restapi --template '{{ printf "%s\n" (.data.clearPassword | base64decode) }}'
```

You should see something like the following screenshot (where Dark Mode was activated!):

![StackGres Web Console](web_console-01.png)


### Scaling the cluster

Let's add a new node to the Postgres cluster. Just edit the `simple.yaml` file and change the number of instances from `2` to `3`:

```yaml
  instances: 3
```

and then apply it:

```
$ kubectl apply -f simple.yaml
sgcluster.stackgres.io/simple configured
```

In a few seconds, a third node would have been spinned up:

```
$ kubectl get pods
NAME       READY   STATUS    RESTARTS   AGE
simple-0   6/6     Running   0          55m
simple-1   6/6     Running   0          74m
simple-2   6/6     Running   0          61s
```

And Patroni should also reflect its status as a new replica:

```
$ kubectl exec -it simple-0 -c patroni -- patronictl list                                                                            
+ Cluster: simple (6979461716096839850) ---+---------+----+-----------+
| Member   | Host                | Role    | State   | TL | Lag in MB |
+----------+---------------------+---------+---------+----+-----------+
| simple-0 | 192.168.40.142:7433 | Leader  | running |  2 |           |
| simple-1 | 192.168.12.150:7433 | Replica | running |  2 |         0 |
| simple-2 | 192.168.71.214:7433 | Replica | running |  2 |         0 |
+----------+---------------------+---------+---------+----+-----------+
```

### 3.5. Cleaning up

Let's now delete the cluster for now, we will create more advanced cluster and configurations in the next section.

```
$ kubectl delete sgcluster simple 
sgcluster.stackgres.io "simple" deleted
```


## 4. Creating a fully-featured cluster

First let's create a namespace to be used for all of the lab objects. Let's call it `demo`:

```
$ kubectl create namespace demo
```

### Instance profiles

StackGres helps you try to enforce good practices. One of such is not to have very heterogeneus database (instance) sizes, but rather to concentrate on a few standardized ones. This is achieved by having the ability to create "t-shirt sizes" for the instances. This is controlled by a CRD called `SGInstanceProfile`.

Create one or more such instance profiles by applying YAML files like:

```yaml
apiVersion: stackgres.io/v1
kind: SGInstanceProfile
metadata:
  namespace: demo
  name: size-small
spec:
  cpu: "4"
  memory: "8Gi"
```

You can list the created (available) instance profiles from the Web Console or via:

```
$ kubectl -n demo get sginstanceprofiles
```


### Custom Postgres configurations

StackGres comes with an expertly tuned Postgres configuration (aka `postgresql.conf`) by default. However, it does not prevent you from using your own specialized Postgres configuration. If you want to create one and need some guidance, consider using the [postgresqlCONF](https://postgresqlco.nf) service, which gives you detailed parameter information in several langauges, recommendations, a tuning guide and even a facility to store and manage your Postgres configurations online.

StackGres hides the implementation details of how Postgres configuration file works, and how to configure it with Patroni. Instead, a specialized CRD is exposed for you to manage Postgres configurations, called `SGPostgresConfig`. Postgres configurations are created (and/or modified) once and then can be used in zero, one or multiple clusters. No need to repeat the configuration in every cluster.

Create the following YAML file and apply. Feel free to modify parameters to your suiting:

```yaml
apiVersion: stackgres.io/v1
kind: SGPostgresConfig
metadata:
  namespace: demo
  name: pgconfig1
spec:
  postgresVersion: "12"
  postgresql.conf:
    work_mem: '16MB'
    shared_buffers: '2GB'
    random_page_cost: '1.5'
    password_encryption: 'scram-sha-256'
    log_checkpoints: 'on'
    jit: 'off'
```

You can see in the _Status_ section of the CRD the injected default values that StackGres tune for you:

```
$ kubectl -n demo describe sgpostgresconfig pgconfig1
```


### Custom connection pooling configuration

Similarly, you may also customize the configuration of StackGres embedded connection pooling (PgBouncer). It is also its own `SGPoolingConfig` CRD, which can be used on zero, one or more clusters, referenced by name. Create an apply the following YAML:

```yaml
apiVersion: stackgres.io/v1
kind: SGPoolingConfig
metadata:
  namespace: demo
  name: poolconfig1
spec:
  pgBouncer:
    pgbouncer.ini:
      pool_mode: transaction
      max_client_conn: '200'
      default_pool_size: '200'
```


### Setting up automated backups

StackGres supports automated backups (based on Postgres continuous archiving, that is base backups plus WAL archiving) and backup lifecycle management. To achieve maximum durability, backups are stored on cloud/object storage, supporting S3, GCP, Azure Blob and S3-compatible object storages.

First you will need to create an object storage bucket and retrieve the appropriate credentials. These credentials need to be stored on a Kubernetes `Secret`. You may use any keys to encode the credentials in the `Secret`, but those same keys will be used within the backup configuration to reference the created Secret.

If you wish to create an S3 bucket, follow the next steps:

* Change to `00-setup_environment/aws/awscli/` folder in this same repository.
* Rename `variables.sample` to `variables` and edit to your convenience.
* Run `00-setup_environment/aws/awscli/` and note the last line of the output. It is a `kubectl` command line that will create the `Secret` that will be referenced to access the bucket, and will have the appropriate credentials setup. It should be similar to `kubectl --namespace demo create secret generic $S3_BACKUP_CREDENTIALS_K8S_SECRET --from-literal="accessKeyId=*****" --from-literal="secretAccessKey=*****`.

Now we can create a backup configuration that will reference the secret. This configuration is yet another CRD, named `SGBackupConfig`, which again may be referenced by name. Create, edit (replace "varialbes) and apply the following YAML:

```yaml
apiVersion: stackgres.io/v1
kind: SGBackupConfig
metadata:
  namespace: demo
  name: backupconfig1
spec:
  baseBackups:
    cronSchedule: '*/5 * * * *'
    retention: 6
  storage:
    type: 's3'
    s3:
      bucket: $bucketName
      awsCredentials:
        secretKeySelectors:
          accessKeyId: {name: '$secretName', key: 'accessKeyId'}
          secretAccessKey: {name: '$secretName', key: 'secretAccessKey'}
```

Please review carefully the backup configuration. Ensure that the bucket name, the name of the secret and the keys to the secret are correct; as well as the secret itself is well-formed and with the correct credentials. If not, cluster creation may fail or may see complex errors. Unfortunately, as of now there's no a priori validation of the correctness of the credentials.


### Creating a distributed logs server

By default StackGres will have Postgres and Patroni logs in the container, which you can get with `kubectl logs`. However, that is not an ideal method to store important logs. StackGres created a technology to store both Patroni and Postgres logs on a distributed (centralized) logs server. Logs from all the pods are sent to this logs server, which is in turn a separate Postgres database using the time-series Timescale extension. A log server may store the logs for zero, one or more StackGres clusters.

Distributed log servers are represented by their own CRD (`SGDistributedLogs`). Create and apply the following YAML file to create a distributed logs server.

```yaml
apiVersion: stackgres.io/v1
kind: SGDistributedLogs
metadata:
  namespace: demo
  name: distributedlogs
spec:
  persistentVolume:
    size: 50Gi
```

The configurations before (instance profile, Postgres, connection pooling and backup configuration) didn't create any "physical" resources, they were only metadata. However, this will create pods:

```
$ kubectl -n demo get pods
NAME                READY   STATUS    RESTARTS   AGE
distributedlogs-0   3/3     Running   0          48s
```

Wait until they are ready to proceed to the next step.


### Creating the more advanced cluster

We're now completely ready to create a more "advanced" cluster. The main differences from the one created before will be:
* That we have explicitly set the size (t-shirt size) of the cluster.
* We have custom Postgres and connection pooling (PgBouncer) configurations.
* It will have automated backups, based on the backup configuration.
* It will export metrics to Prometheus automatically. Grafana dashboards will be visible from the embedded pane in the Web Console.
* Logs will be sent to the distributed logs server, which will in turn show them in the Web Console.

Create and apply the following YAML file:

```yaml
apiVersion: stackgres.io/v1
kind: SGCluster
metadata:
  namespace: demo
  name: hol
spec:
  postgresVersion: '12.6'
  instances: 2
  sgInstanceProfile: 'size-small'
  pods:
    persistentVolume:
      size: '10Gi'
  configurations:
    sgPostgresConfig: 'pgconfig1'
    sgPoolingConfig: 'poolconfig1'
    sgBackupConfig: 'backupconfig1'
  distributedLogs:
    sgDistributedLogs: 'distributedlogs'
  prometheusAutobind: true
```

You may notice that in this ocassion the pods contain one extra container. This is due to the agent (FluentBit) used to export the logs to the distributed logs server. You can check both from `kubectl -n demo get pods`:

```
$ kubectl -n demo get pods
NAME                          READY   STATUS    RESTARTS   AGE
distributedlogs-0             3/3     Running   0          3m16s
hol-0                         7/7     Running   0          98s
hol-1                         7/7     Running   0          72s
```

as well as `kubectl -n demo describe sgcluster hol` and the Web Console the status of the newly created cluster.


### Accesing the Postgres database via the services

StackGres creates, by default, two Kubernetes services to expose the Postgres cluster: one for reads and writes (`-primary`) and one for reads (`-replicas`), which is load balanced across all replicas. The name of the services is the name of the cluster plus one of the two suffixes (e.g. `hol-primary`).

Previously, we connected to the Postgres server by executing `psql` from the `postgres-util` container. That's fine for administration purposes, but users of the database will likely connect via the Postgres protocol from other pods. For this, they will use these services as the endpoint to which to connect. You could use here any application that supports Postgres. For the purpose of this lab, we will again use `psql`, but run from a separate pod that will contain a container that in turn contains `psql`. For example, the very same `postgres-util` container of the StackGres project --but run externally to the StackGres cluster, as a separate pod. Run:

```
$ kubectl -n demo run psql --rm -it --image ongres/postgres-util:v13.2-build-6.2 --restart=Never -- psql -h hol-primary postgres postgres
```

It will ask for a password. Probably you don't need it, so let it fail (e.g. type any password). It will abort. This time we're connecting via Postgres wire protocol, and StackGres configures Postgres to require authentication. The `postgres` (superuser account in Postgres) password is generated randomly when the cluster is created. You can retrieve it from the appropriate secret, named as the cluster, and obtaining the key `"superuser-password"` (note the quoting, it is required as it is using a dash).

```
$ kubectl -n demo get secret hol --template '{{ printf "%s" (index .data "superuser-password" | base64decode) }}
``` 

Now you should be able to connect and operate normally. Also to the `-replicas` service, by using `-h hol-replicas` in the `psql` connection string. Your applications are now ready to use Postgres!


### Accesing Postgres and Patroni logs

It's easy: go to the Web Console, to the cluster, and click on the `Logs` pane. But now, let's do from the CLI. Because StackGres stores Postgres logs on a (separate!) Postgres database, enhanced by the time-series TimescaleDB extension, you are able to connect to the database and query the logs with SQL! Indeed, the `SGDistributedLogs` CR that we created before led to the creation of a specialized `SGCluster`, used for logs. So how to connect to it? The same way we just connected to the main cluster: through the `-primary` service, where in this case the host is obviously the `distributedlogs` name that we use for the `SGDistributedLogs` CR. Similarly, we can retrieve the connection password from the equivalent secret:

```
$ kubectl -n demo get secret distributedlogs --template '{{ printf "%s" (index .data "superuser-password" | base64decode) }}'
```

```
$ kubectl -n demo run psql --rm -it --image ongres/postgres-util:v13.2-build-6.2 --restart=Never -- psql -h distributedlogs-primary postgres postgres
```

Now that we're in `psql`, we can query the logs with SQL. Let's do that! The following sequence of commands will allow you to list the databases (there will be one per every cluster that is sending logs to this distributed logs server, named `$namespace_$cluster`), connect to the database for our current cluster, count the Postgres log entries, describe the logs table and select all logs of type `ERROR` (you can generate such logs, if you don't have any, by typing any SQL error into the SQL console --of the source cluster, not this one; bear in mind that logs take a few seconds to propagate, may not appear instantly).

```sql
\l+
\c demo_hol
\dt log_postgres
select count(*) from log_postgres;
select * from log_postgres where error_severity = 'ERROR';
```


### Let's add some extensions!

Postgres extensions are awesome. Possibly, one of the most appreciated Postgres features.

Let's first connect to the `hol` cluster, and run a command to list the extensions available:

```sql
postgres=# select * from pg_available_extensions();
        name        | default_version |                           comment                            
--------------------+-----------------+--------------------------------------------------------------
 dblink             | 1.2             | connect to other PostgreSQL databases from within a database
 plpgsql            | 1.0             | PL/pgSQL procedural language
 pg_stat_statements | 1.7             | track execution statistics of all SQL statements executed
 plpython3u         | 1.0             | PL/Python3U untrusted procedural language
(4 rows)
```

That looks like a very limited set of extensions available. Can we do better? Fortunately, yes. StackGres has developed a very innovative mechanism to load Postgres extensions on-demand, onto the (otherwise immutable!) container images. It is quite simple to use. Let's load the `timescaledb` extension into our cluster. Edit the cluster's YAML file, adding the following section:

```yaml
  postgresExtensions:
    - name: ltree
```

(note the indentation; it's 1 level, 2 spaces). After a few seconds, the extension would have been dynamically downloaded onto the container, and will now show in the Postgres console if we repeat the previous query.

You just need now to run `create extension ltree;` on those databases that you want the extension to be installed and will be ready to go!

To check the currently available extensions and versions, please check the Web Console. Please also note that installing some extensions may require a cluster restart. This restriction will be lifted in future versions.


### Let's run a benchmark!

StackGres also supports automating "Day 2 Operations". This is performed via a CRD called `SGDbOps`. `SGDbOps` support several kind of operations, with more added in future versions. Let's try one of them, that runs a benchmark. Create and apply the following YAML file:

```yaml
apiVersion: stackgres.io/v1
kind: SGDbOps
metadata:
  namespace: demo
  name: pgbench1
spec:
  op: benchmark
  sgCluster: hol
  benchmark:
    type: pgbench
    pgbench:
      databaseSize: 1Gi
      duration: P0DT0H2M
      usePreparedStatements: false
      concurrentClients: 20 
      threads: 8 
```

Upon submitting this CR, StackGres will schedule and run a benchmark. The results of the benchmark will be written in the `.Status` field of the CRD, which you can query with `kubectl describe`. You may also check them from the Web Console.
